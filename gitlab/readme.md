# notes on linking gitlab and k8s

- https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster
- need to get the token right
- I would like to use ingress...? https://stackoverflow.com/questions/52895437/is-it-possible-to-access-the-kubernetes-api-via-https-ingress
- https://stackoverflow.com/questions/60459767/cant-display-dashboard-through-api-traefik-v2


## Ingress rule for the kubernetes API

- [gitlab add existing cluster docs](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster)
  - [gui](https://gitlab.com/groups/greenhouseaffectors/-/clusters/93629)

- [`/etc/kubernetes/pki`](https://stackoverflow.com/questions/47935675/k8s-1-9-how-to-access-api-server-with-client-certifiicate) is where the kubernetes certs are
- [first question](https://stackoverflow.com/questions/52895437/is-it-possible-to-access-the-kubernetes-api-via-https-ingress)
- 6443
- traefik = nodeport
- [traefik ingress tls](https://docs.traefik.io/v1.7/user-guide/kubernetes/#add-a-tls-certificate-to-the-ingress)

### Actual steps

- First, add a secret from `./pki` not sure which file yet.

`kubectl create secret tls kubernetes-api-cert --key=ca.key --cert=ca.crt`

- Then setup the ingress from [here](https://docs.traefik.io/v1.7/user-guide/kubernetes/#add-a-tls-certificate-to-the-ingress)

### cert stuff

- Using the following steps from [this doc](https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/#without-kubectl-proxy) I can get to the API using curl with `--insecure`

```
APISERVER=https://miketheadmin.myddns.me:6443
SECRET_NAME=$(kubectl get secrets | grep ^default | cut -f1 -d ' ')
TOKEN=$(kubectl describe secret $SECRET_NAME | grep -E '^token' | cut -f2 -d':' | tr -d " ")

curl $APISERVER/api --header "Authorization: Bearer $TOKEN" --insecure
```

- adding the cert gitlab asked for results in `SSL: no alternative certificate subject name matches target host name 'example.com'`
  - https://serverfault.com/questions/950420/ssl-no-alternative-certificate-subject-name-matches-target-host-name
  - Can't find docs on how I did this...


```sh
export CERT=$(kubectl get secret default-token-cqfkw -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
curl $APISERVER/api --header "Authorization: Bearer $TOKEN" --insecure
```

- Replace `<your master's IP address` in the following line in your `~/.kube/config` with your external IP address with port `6443` forwarded to your master node:

`server: https:<your master's IP address>:6443`

- If this works, that's great. If you get an error similar to the following, continue reading:

```
$ kubectl get po
Unable to connect to the server: x509: certificate is valid for new-host-10, kubernetes, kubernetes.default, kubernetes.default.svc, kubernetes.default.svc.cluster.local, not <your hostname>.com
```

- First, change the config back to the IP address, then make the following changes, and change it back.

- Read through [this](https://codefarm.me/2019/02/01/access-kubernetes-api-with-client-certificates/)
- Adding an fdqn to a cert https://stackoverflow.com/questions/46360361/invalid-x509-certificate-for-kubernetes-master/54664423#54664423

## actual steps

- Your best bet would have been to add the fdqn at install in the kubic settings but they don't have this setting. You have to add it after [as described here](https://blog.scottlowe.org/2019/07/30/adding-a-name-to-kubernetes-api-server-certificate/)

- Had to add metallb (with an IP range of 192.168.1.128/29) to manage gitlab's nginx ingress controller's IP address (because it acts as a loadbalancer service).
  - `kubicctl deploy metallb 192.168.1.128/29`
  - Also added a group in the my fios router to handle this subnet (firewall rules...)

## Gitlab stuff

- Add the service account

# Hosting multiple websites

- To host multiple websites:
  1. Link the cluster to a group
  2. Add additional hosts  (adds an ingress rule) [in this file](https://gitlab.com/greenhouseaffectors/php/-/blob/development/.gitlab/auto-deploy-values.yaml)