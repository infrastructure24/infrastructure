# Installing Nextcloud

- Helmchart used: https://github.com/nextcloud/helm/blob/master/charts/nextcloud/values.yaml

# OIDC

- OIDC app used: https://github.com/pulsejet/nextcloud-oidc-login
- config.php file location: `/var/www/html/config/config.php`
- 