# Install the CustomResourceDefinition resources separately

```kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.4/cert-manager.crds.yaml```


- **Important:** If you are running Kubernetes v1.15 or below, you will need to add the `--validate=false` flag to your kubectl apply command, or else you will receive a validation error relating to the x-kubernetes-preserve-unknown-fields field in cert-manager’s CustomResourceDefinition resources. This is a benign error and occurs due to the way kubectl performs resource validation.

# Create the namespace for cert-manager

```
kubectl create namespace cert-manager
```

# Add the Jetstack Helm repository

```
helm repo add jetstack https://charts.jetstack.io
```

# Update your local Helm chart repository cache

```
helm repo update
```

# Install the cert-manager Helm chart

```
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v1.0.4
```

# Lets-encrypt issuer

```
kubectl apply -n cert-manager -f ./letsencrypt_issuer.yaml
```

# using the issuer

- https://cert-manager.io/docs/tutorials/acme/ingress/#step-6-configure-let-s-encrypt-issuer
- note it's important to use the hosts field in the ingress resource instead of just the secret name field :(
- don't automatically redirect http to https https://stackoverflow.com/questions/58284860/404-challenge-response-with-cert-manager-and-traefik-ingress
  - `--entrypoints.websecure.http.tls.certResolver=leresolver`
  - https://doc.traefik.io/traefik/routing/entrypoints/#tls
- https://github.com/jetstack/cert-manager/issues/2517