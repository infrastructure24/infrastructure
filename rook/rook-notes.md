# Rook pre-reqs

- Rook requires unformated volumes to work.
  - For this reason, and because ceph is difficult to manage, I'm using longhorn instead.

# rook notes

- I installed rook-ceph using [this link](https://rook.io/docs/rook/v1.2/ceph-quickstart.html)
- The following files are from [this repo](https://github.com/rook/rook)
- then I created storage classes using the files in `rook/cluster/examples/kubernetes/ceph`
- Then I made block the default storage class using `rook/cluster/examples/kubernetes/ceph/csi/rbd/storageclass.yaml`
- Then I set this storage class as the default storage class with `kubectl patch sc rook-ceph-block -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'`
  - [these docs](https://kubernetes.io/docs/tasks/administer-cluster/change-default-storage-class/) told me how to do this

- [rook backups with k10](https://blog.kasten.io/posts/backup-and-disaster-recovery-for-rook-ceph-with-kasten-k10/)
  - [another k10 link](https://www.youtube.com/watch?v=hQ_f96vlGNQ)


- CHANGE NUMBER OF MONS IN CEPHCLUSTER TO 1 (if you have < 3 worker nodes)
  - true

- use cephfs as the default storage class because it can do RWX


- Steps to restart ceph:

```
thor:~ # rm -rf /var/lib/rook
thor:~ # dmsetup remove /dev/dm-0 && dmsetup remove /dev/dm-1 && dmsetup remove /dev/dm-2
thor:~ # sgdisk --zap-all /dev/sdc && sgdisk --zap-all /dev/sda && sgdisk --zap-all /dev/sdd
```


- I was having [this issue](https://github.com/rook/rook/issues/6900), but fixed it by using ceph 1.5.3 instead of 1.5.5.
- [DO nginx with cert-manager](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes)