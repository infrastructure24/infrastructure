# cockpit

- Cockpit is the runtime replacement for yast. It is installed from yast or with zypper, and can be used to manage your nodes.

# Install cockpit

- zypper commands for installing cockpit (might require some):

```
$ transactional-update shell
$ zypper search cockpit
$ zypper in -t pattern microos_cockpit
$ zypper in cockpit-storaged cockpit-tests cockpit-podman cockpit-networkmanager cockpit-system cockpit-ws
$ exit
$ reboot
```

# changing the cockpit port from 9090 to something else

- NOTE: This caused problems because cockpit can't serve on 2 ports at once. The way to fix this is to enable the cockpit socket, then reboot
  - [see here for this issue](https://github.com/cockpit-project/cockpit/issues/12584)
  - [cockpit docs](https://cockpit-project.org/guide/149/listen.html)
  
- see if something is running on a port 9090 with:

```
lsof -i :9090
```

- if something is running:

```
$ mkdir -p /etc/systemd/system/websocket.cockpit.d/
$ vim /etc/systemd/system/websocket.cockpit.d/listen.conf 
```

- Enter this info

```
[Socket]
ListenStream=9898
```

# running cockpit

```
sudo systemctl enable cockpit.socket
sudo systemctl start cockpit.socket
```