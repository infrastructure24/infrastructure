# Install Rancher

- [rancher HA install docs](https://rancher.com/docs/rancher/v2.x/en/installation/install-rancher-on-k8s/)
- [rancher helm options](https://rancher.com/docs/rancher/v2.x/en/installation/resources/chart-options/#helm-chart-repositories)
- [helm chart](https://github.com/rancher/rancher/tree/master/chart)

## Pre-reqs

- DOESN'T SUPPORT k8s 1.20
- When provisioning nodes and the cluster:
  - install open-iscsi on all nodes
  - Mount an ext4 partition to [`/var/lib/rancher`](https://longhorn.io/docs/0.8.0/users-guide/multidisk/) on all nodes
  - enable mount propagation on the cluster

- [longhorn pre-reqs page](https://longhorn.io/docs/1.0.2/deploy/install/#installation-requirements)

## Install Cert-Manager

- [install cert-manager](../cert-manager/Readme.md)

## Helm install

- [helm options](https://rancher.com/docs/rancher/v2.x/en/installation/resources/chart-options/)


```sh
$ helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
$ kubectl create namespace cattle-system
$ helm install rancher --namespace=cattle-system -f ./rancher-values.yaml rancher-stable/rancher
```
