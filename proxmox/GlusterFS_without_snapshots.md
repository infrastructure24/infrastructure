# GlusterFS

- https://www.youtube.com/watch?v=2A25Zgs4jOQ

- __WARNING__: XFS requires a battery backup (UPS)
- GlusterFS is an easy to use a distributed file system that we will link to Kubernetes through a storage class.
  - GlusterFS simpler than Ceph and has lower resource requirements.
  - This setup shouldn't require large amounts of additional RAM like a ZFS setup would.
  - Distributed file systems allow you to use storage from any machine on any other machine.
  - Later we will create backups through AWS.
  - Gluster Quickstart: https://docs.gluster.org/en/latest/Quick-Start-Guide/Quickstart/
- 3 parts:
  1. Setting up Disks
  2. Setting up LXC Container and Mounting Disks into it
  3. Installing Gluster inside the LXC Container

## __Part 1__: Setting up Disks

- https://www.youtube.com/watch?v=BkI1lfiuKgY

### Wiping the disks

- Use `fdisk -l` to see what disks you have.
- for information about the disks:

```sh
smartctl -i --test=short /dev/sda
```

- Unmount old file systems on the glusterFS disks with `umount /dev/mapper/...`
- Use `wipefs -a /dev/sdd` to remove old file systems.
- Remove old partitions with `sgdisk --zap-all /dev/sdX`
- partition with `gdisk` then `o` then `w`

### Create a VM and LXC Container Storage

- Dedicate a disk entirely to storing VM images and LXC container templates.
  - Easily done through GUI.
  - `pvesm lvmscan`

### Formatting Disks with XFS

- using `/mnt/bindmounts` for security according to proxmox docs. Don't use the GUI!
- Steps from gluster quickstart.

```
# mkfs.xfs -i size=512 /dev/sdc
# mkdir --parents /mnt/bindmounts_gluster/gluster01
# echo '/dev/sda /mnt/bindmounts_gluster/gluster01 xfs defaults 1 2' >> /etc/fstab
# mount -a && mount
# findmnt
```

## __Part 2__: Setting up LXC Container and Mounting Disks into it

- https://studio.youtube.com/video/JTBL765jFg8/edit
- LXC is like Docker but designed to be persistent while Docker is designed to be ephemeral. Proxmox supports LXC natively.
- CentOS on LXC instead of VM to save resources.
- Not installed on k8s, so if k8s goes down storage still up. Not installed on the host, so if the LXC container goes down Proxmox is still up.

### Create LXC container for Gluster

- Download the image

```
pveam update
pveam available --section system
pveam download local centos-8-default_20201210_amd64.tar.xz
pveam list local
```

- Create an LXC container from the image *with flags* using `pct create <VMID> <OS template>`

 <!-- --net[n] name=<string> [,bridge=<bridge>] [,firewall=<1|0>] [,gw=<GatewayIPv4>] [,gw6=<GatewayIPv6>] [,hwaddr=<XX:XX:XX:XX:XX:XX>] [,ip=<(IPv4/CIDR|dhcp|manual)>] [,ip6=<(IPv6/CIDR|auto|dhcp|manual)>] [,mtu=<integer>] [,rate=<mbps>] [,tag=<integer>] [,trunks=<vlanid[;vlanid...]>] [,type=<veth>]  -->

```
# pct list
# pct create 101 local:vztmpl/centos-8-default_20201210_amd64.tar.xz --features mount=xfs --ostype centos --storage=CTVM --unprivileged=0  --description "centos glusterfs" --hostname thorgluster --password "password" --onboot 1 --net0 name=eth0,bridge=vmbr0
```

- Notes on parameters
  - `--features mount=xfs` tells proxmox to use xfs as the default filesystem when mounting drives
    - `--features nesting=1` is needed on some distros, but I don't think we need it here.
  - `--storage=CTVM` determines where the LXC container is stored (can be seen with `pvesm lvmscan`)
    - CTVM is an LVM volume, which works with LXC and VMs.
  - `--unprivileged=0` makes the container a privileged container. This is required when bind mounting because the host owns the directory.
  - `--onboot 1` makes proxmox start this container when it boots up.
  - `--net0 name=eth0,bridge=vmbr0` sets up a bridge network for the container.

### Bind Mount XFS to LXC

- What is bind mounting?
  - 3 types of mounting:
    - __Bind mount__: for accessing arbitrary directories on the host from a container.
    - __Storage backed__: for accessing specific directories managed by proxmox from a container
    - __Device mount__: for mounting block devices from the host directly into the container.
  - Storage backed would be ideal, but you can only mount it as ext4 and glusterfs requires xfs.

- Bind Mount disks into containers using `pct set <VMID>` with this flag `-mp<n> mp=/container/mount/point,/host/dir`


```
# pct set 101 -mp1 mp=/mountpoint/gluster01,/mnt/bindmounts_gluster/gluster01
# pct set 101 -mp2 mp=/mountpoint/gluster02,/mnt/bindmounts_gluster/gluster02
```

- confirm that the image is available in the container

```
# pct start 101
# pct enter 101
# findmnt
```

### Mount LVM for Gluster Snapshots

- This is unsafe but enables gluster to manage snapshots
- DO NOT DO THIS:

```
lxc.cgroup2.devices.allow = b 8:48 rwm
lxc.mount.entry = /dev/ dev/ none bind,create=file 0 0 
```

- However mounting /dev/dm-0 (or whatever it is) and mounting /dev/mapper/ID works...??? However that's populated at runtime
- You migh


### References

- https://pve.proxmox.com/pve-docs/pct.1.html
- https://pve.proxmox.com/pve-docs/chapter-pct.html#pct_container_images
- https://pve.proxmox.com/pve-docs/pve-admin-guide.html
- https://pve.proxmox.com/pve-docs/chapter-pct.html#pct_mount_points
- https://forum.proxmox.com/threads/networking-doesnt-work-on-fedora-lxc-container.108537/
  - Issues on Fedora

## __Part 3__: Installing Gluster inside LXC Container

- Before we begin, take a backup of the LXC container.
  - TODO: use the [proxmox-backup-client](https://pbs.proxmox.com/docs/command-syntax.html) cli command to do this

### Add CentOS Repos

```
# pct enter 101
cd /etc/yum.repos.d/
sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
dnf update
```

- https://forums.centos.org/viewtopic.php?t=78708


### Install gluster

- Proxmox backs up LXC, but doesn't backup drives gluster stores data on so we will mirror our drives. In another tutorial we will create off-site backups (S3 or gluster georeplication).
- [dnf-plugins-core](https://stackoverflow.com/questions/40937056/dnf-missing-config-manager-command) should add the [config-manager](https://kifarunix.com/install-and-setup-glusterfs-storage-cluster-on-centos-8/)
- gluster tut [here](https://www.itzgeek.com/how-tos/linux/centos-how-tos/install-and-configure-glusterfs-on-centos-7-rhel-7.html) also quickstart

```
dnf install dnf-plugins-core
dnf update
dnf install centos-release-gluster
vi ./CentOS-Gluster-9.repo
# replace mirrorlist with vault
dnf swap centos-linux-repos centos-stream-repos
dnf swap centos-linux-repos centos-stream-repos
reboot
dnf install glusterfs glusterfs-fuse glusterfs-server
service glusterd start
# service glusterd status
# gluster volume create gv0 replica 2 thorgluster:/mountpoint/gluster01 thorgluster:/mountpoint/gluster02
# gluster volume start gv0
# gluster volume info
```

### Test the GlusterFS volume

- TODO


### Adding Another Node

```
# iptables -I INPUT -p all -s 192.168.1.227 -j ACCEPT
# gluster peer probe server2
# gluster peer status
```