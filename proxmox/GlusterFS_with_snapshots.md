# GlusterFS with Snapshots

- https://youtu.be/kyxo0nuySZE
- __WARNING__: XFS requires a battery backup (UPS)
- GlusterFS is an easy to use a distributed file system that we will link to Kubernetes through a storage class.
  - GlusterFS simpler than Ceph and has lower resource requirements.
  - This setup shouldn't require large amounts of additional RAM like a ZFS setup would.
  - Distributed file systems allow you to use storage from any machine on any other machine.
  - Snapshots. Georeplication in a future tutorial maybe
  - Gluster Quickstart: https://docs.gluster.org/en/latest/Quick-Start-Guide/Quickstart/
- 5 parts:
  1. Setting up LXC Container
  2. Device Mounting Disks to LXC
  3. LVM and XFS on Disks within LXC
  4. Installing Gluster inside the LXC Container
  5. Snapshot Demo

## __Part 1__: Setting up LXC Container

- https://youtu.be/TxesPwAeJVI
- LXC is like Docker but designed to be persistent while Docker is designed to be ephemeral. Proxmox supports LXC natively.
- LXC instead of VM to save resources.
- Not installed on k8s, so if k8s goes down storage still up. Not installed on the host, so if the LXC container goes down Proxmox is still up.

### Create a VM and LXC Container Storage

- Dedicate a disk entirely to storing VM images and LXC container templates.
  - Easily done through GUI so I won't show steps here.
  - `pvesm lvmscan` will show the name of storage options.

### Create LXC container for Gluster

- Download the image

```
pveam update
pveam available | grep core
pveam download local debian-10-turnkey-core_16.1-1_amd64.tar.gz
pveam list local
```

- Create an LXC container from the image *with flags* using `pct create <VMID> <OS template>`

 <!-- --net[n] name=<string> [,bridge=<bridge>] [,firewall=<1|0>] [,gw=<GatewayIPv4>] [,gw6=<GatewayIPv6>] [,hwaddr=<XX:XX:XX:XX:XX:XX>] [,ip=<(IPv4/CIDR|dhcp|manual)>] [,ip6=<(IPv6/CIDR|auto|dhcp|manual)>] [,mtu=<integer>] [,rate=<mbps>] [,tag=<integer>] [,trunks=<vlanid[;vlanid...]>] [,type=<veth>]  -->

```
# pct list
# pct create 101 local:vztmpl/debian-10-turnkey-core_16.1-1_amd64.tar.gz --features mknod=1,nesting=1,fuse=1 --ostype debian --storage=lvmthinCTVM --description "debian turnkey glusterfs" --hostname thorgluster --password "password" --onboot 1 --net0 name=eth0,bridge=vmbr0 --unprivileged=0
```

- Notes on parameters
  - `--features nesting=1` allows using containers within containers. Not sure if gluster uses this.
  - `--features mknod=1` because gluster uses the mknod command.
  - `--features fuse=1` gluster uses fuse. FUSE is "File System in Userspace" see the [gluster docs on fuse](https://glusterdocs-beta.readthedocs.io/en/latest/overview-concepts/fuse.html).
  - `--storage=lvmthinCTVM` determines where the LXC container is stored (can be seen with `pvesm lvmscan`)
  - `--onboot 1` makes proxmox start this container when it boots up.
  - `--net0 name=eth0,bridge=vmbr0` sets up a bridge network for the container.
  - `--unprivileged=0` needed for `lxc-device add`. Unprivileged lxc may work when you [add udev rules like this](https://forum.proxmox.com/threads/lxc-passthrough-to-already-running-container.56312/)

- Before we begin, take a backup of the LXC container.
  - TODO: use the [proxmox-backup-client](https://pbs.proxmox.com/docs/command-syntax.html) cli command to do this

### References

- https://pve.proxmox.com/pve-docs/pct.1.html
- https://pve.proxmox.com/pve-docs/chapter-pct.html#pct_container_images
- https://pve.proxmox.com/pve-docs/pve-admin-guide.html
- https://pve.proxmox.com/pve-docs/chapter-pct.html#pct_mount_points
- https://forum.proxmox.com/threads/networking-doesnt-work-on-fedora-lxc-container.108537/
  - Issues on Fedora

## __Part 2__: Device Mounting Disks and LVM to LXC

- https://youtu.be/QK7niV8UeaI
- Mounting devices and LVM to LXC container because gluster snapshots use LVM thin.

### Wiping the disks

- Use `fdisk -l` to see what disks you have.
- for information about the disks:

```sh
smartctl -i --test=short /dev/sdc
```

- Unmount old file systems on the glusterFS disks with `umount /dev/mapper/...`
- Use `wipefs -a /dev/sdd` to remove old file systems.
- Remove old partitions with `sgdisk --zap-all /dev/sdX`
- Don't repartition. We will do that inside the container
- Remove LVM stuff with `pvremove`, `lvremove` and `vgremove`

### LXC Device Mount

- What is Device mounting?
  - 3 types of mounting:
    - __Bind mount__: for accessing arbitrary directories on the host from a container.
    - __Storage backed__: for accessing specific directories managed by proxmox from a container
    - __Device mount__: for mounting block devices from the host directly into the container.

- We're device mounting along with some extra stuff because gluster snapshots use LVM.

```
udevadm info /dev/sdc
```

- Edit `/etc/pve/lxc/101.conf` and add the following:

```
lxc.autodev: 1
lxc.cgroup2.devices.allow: a
lxc.mount.entry: /dev/disk/by-id/ata-ST2000DM008-2FR102_ZFL1MDSK dev/sdc none bind,create=file 0 0
lxc.mount.entry: /dev/mapper/control dev/mapper/control none bind,create=file 0 0
lxc.mount.entry: /run/lvm run/lvm none bind,create=file 0 0
```

- Notes on these configurations:
  - `/dev/mapper/control` is a __CHARACTER__ device NOT a block device
  - Mounting with uuid because `/dev/sdc` [can change](https://unix.stackexchange.com/questions/3158/hard-drive-device-partition-naming-convention-in-linux#3160) but uuid doesn't.
  - I was getting errors mounting `/run/lvm` on a centos container but not on debian turnkey.

### Disable `udev` inside LXC

- Enter the container, edit `/etc/lvm/lvm.conf` and change the following lines from 1 to 0:

```
udev_sync = 0
udev_rules = 0
```

### Make sure it Works

- Run the following inside the container

```
fdisk -l
pvscan
```

### Afterthoughts

- I had issues using specifying the Maj:Min when allowing devices. I tried `lxc.cgroup2.devices.allow = c *:* rwm` for `/dev/mapper/control` and `lxc.cgroup2.devices.allow = b 8:20 rwm` for `/dev/sdc` (where `8:20` is from `stat /dev/sdc`) so I allowed all devices. Ideally I would have limited these devices.
  - "The major part identifies the device type (IDE disk, SCSI disk, serial port, etc.) and the minor identifies the device (first disk, second serial port, etc.). Most times, the major identifies the driver, while the minor identifies each physical device served by the driver." [Maj:Min docs](https://linux-kernel-labs.github.io/refs/heads/master/labs/device_drivers.html)


### References

- [issue to answer](https://github.com/lxc/lxc/issues/960)

- Key resources:
  - [cgroups1 vs cgroups2 docs](https://chrisdown.name/talks/cgroupv2/cgroupv2-fosdem.pdf)
    - [issue if you use the wrong one](https://forum.proxmox.com/threads/pve-7-0-lxc-intel-quick-sync-passtrough-not-working-anymore.92025/#post-400916)
  - [lxc.container.conf docs](https://linuxcontainers.org/lxc/manpages/man5/lxc.container.conf.5.html)
  - [We need to mount /run/lvm into the container](https://unix.stackexchange.com/questions/105389/arch-grub-asking-for-run-lvm-lvmetad-socket-on-a-non-lvm-disk)
  - [udev_sync](https://serverfault.com/questions/802766/calling-lvcreate-from-inside-the-container-hangs)

- Things that are similar to what I did:
  - [non-proxmox lxc device passthrough](https://github.com/lxc/lxd/pull/6429)
  - [usb mounting in proxmox](https://gist.github.com/Yub0/518097e1a9d179dba19a787b462f7dd2#gistcomment-2625690)
  - [generic device passthrough on proxmox forum](https://forum.proxmox.com/threads/lxc-and-device-passthrough.26967/)


- Things I didn't use but are good to know:
  - [lxc.apparmor.profile](https://forum.proxmox.com/threads/container-with-physical-disk.42280/)
  - [udev rules docs](http://reactivated.net/writing_udev_rules.html)
  - [autodev and hook version](https://gist.github.com/Waltibaba/93d32e32513cb2d6dbab)
  - `lxc-device add -n 101 /dev/sdd`

## __Part 3__: LVM and XFS on Disks within LXC

### LVM thin provision disks

- Do this from inside the container
- Needed so we can make snapshots at the Gluster level.
- TODO [see here](https://rajeshjoseph.gitbooks.io/test-guide/content/features/chap-Managing_Snapshots.html)

- https://docs.gluster.org/en/latest/Administrator-Guide/Managing-Snapshots/

```
# pvcreate /dev/sdc
# vgcreate gluster-sdc-vgrp /dev/sdc
# lvcreate -L 1.0T --thin gluster-sdc-vgrp/gluster-sdc-thpl --chunksize 256k --poolmetadatasize 16G  --zero n
# lvcreate --virtualsize 1.0T --thin gluster-sdc-vgrp/gluster-sdc-thpl --name gluster-sdc-thvl
```

<!-- vgchange -a y vgrp (https://unix.stackexchange.com/questions/11125/lvm-devices-under-dev-mapper-missing) -->

- Can see in proxmox GUI

- `/dev/gluster-sdc-vgrp` and its subdirectories are created on the host, but not the container

<!-- 
This line is useful for storage mounting but not for what we're doing here.
pvesm add lvmthin glusterlvmthinsdd --thinpool thpl --vgname vgrp
-->

### Mount Volume Group to LXC with autofs

- Find the directory of the container

- install autofs

`sudo apt-get install autofs`

- add the following lines to /etc/autofs.conf:

```
browse_mode = yes
logging = debug
```

- `mkdir /mnt/lxc-gluster-sdc/mountTo`
- `vim /etc/auto.master` add `/mnt/lxc-gluster-sdc/ /root/auto.gluster-lxc`
- `vim /root/auto.gluster-lxc` add `mountTo --fstype=auto,rw :/root/mountFrom/mountFrom`
  - todo: change this to * and &
- `systemctl restart autofs`

#### other

TODO: delete

```
root@thor:/mnt# mount /dev/dm-12 /mnt/lxc-gluster-sdc/


root@thor:/mnt# ls -al lxc-gluster-sdc/
total 4
drwxr-xr-x 2 root root    6 Jun  7 20:49 .
drwxr-xr-x 6 root root 4096 Jun 26 06:47 ..
```

- mounting without specifying bind or anything... but it shows up as a directory not a block device

#### automount resources

- [redhat docs](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/nfs-autofs)
- man pages: autofs, auto.master, autofs.conf
- [example from arch forum](https://bbs.archlinux.org/viewtopic.php?id=186254)
- [autofs kernel docs](https://www.kernel.org/doc/html/latest/filesystems/autofs-mount-control.html)
- [ubuntu docs](https://help.ubuntu.com/community/Autofs)
  - wildcard too
  - maybe fuse fstype


### Create XFS on LVM

- run the following ON THE HOST
  - maybe inside the container? `apt-get install xfsprogs`
- XFS is best for large files, also gluster uses XFS
- TODO: change `/mnt/lxc-gluster/gluster-sdc` to brick1

```
# mkfs.xfs -i size=512 /dev/gluster-sdc-vgrp/gluster-sdc-thvl
# mkdir --parents /mnt/lxc-gluster/gluster-sdc
```
<!-- 
# echo '/dev/gluster-sdc-vgrp/gluster-sdc-thvl /mnt/lxc-gluster/gluster-sdc xfs defaults 1 2' >> /etc/fstab
# mount -a && mount -->


- add the following line to `/etc/pve/lxc/101.conf`

```
lxc.mount.entry: dev/gluster-sdc-vgrp/gluster-sdc-thvl mnt/lxc-gluster/gluster-sdc xfs relative,defaults 1 2
```

- restart the container and run:

```
# findmnt
```

### Mount XFS to LXC

- Enter the container and create `/mnt/gluster-sdc` with `mkdir` then add the following line to the conf

```
lxc.mount.entry: /mnt/lxc-gluster/gluster-sdc mnt/gluster-sdc none bind,ro
```

### Test LVM snapshots

```
touch /mnt/gluster-sdc/before_snapshot
```

```
lvscan
lvcreate --size 150 --snapshot --name backup gluster-sdc-vgrp/gluster-sdc-thvl
lvscan
```

```
mv /mnt/gluster-sdc/before_snapshot /mnt/gluster-sdc/after_snapshot
```

- restore

```
pass
```

- TODO: test restoring, then delete the old snapshot


### Formatting Disks with XFS

- Steps from gluster quickstart.

```
pvcreate /dev/sdd
vgcreate dummyvg /dev/sda1
lvcreate -L 1T -T dummyvg/dummypool -c 256k --poolmetadatasize 16G  --zero n
lvcreate -V 1G -T dummyvg/dummypool -n dummylv
mkfs.xfs -f -i size=512 -n size=8192 /dev/dummyvg/dummylv
```

```
# mkfs.xfs -i size=512 /dev/sdc
# mkdir --parents /mnt/bindmounts_gluster/gluster01
# echo '/dev/sda /mnt/bindmounts_gluster/gluster01 xfs defaults 1 2' >> /etc/fstab
# mount -a && mount
# findmnt
```

### References

- [LVM snapshots reference](https://kerneltalks.com/disk-management/how-to-guide-lvm-snapshot/)
- [xfs docs](https://www.usenix.org/system/files/login/articles/140-hellwig.pdf)
- [vgchange -a y <name of volume group>](https://unix.stackexchange.com/questions/11125/lvm-devices-under-dev-mapper-missing)
- [redhat restore snapshots](https://access.redhat.com/documentation/en-us/red_hat_gluster_storage/3.5/html/administration_guide/ch08s10)
- [autofs kernel docs](https://www.kernel.org/doc/html/latest/filesystems/autofs.html)
- [autofs variable substitution](https://unix.stackexchange.com/questions/60604/using-autofs-to-mount-under-each-users-home-directory#60609)
- [autofs ubuntu docs](https://help.ubuntu.com/community/Autofs)
- [SO](https://serverfault.com/questions/1050690/why-directories-mounted-with-autofs-are-not-visible-in-filesystem-but-are-acces)

## __Part 4__: Installing Gluster inside LXC Container


### Install gluster

- Proxmox backs up LXC, but doesn't backup drives gluster stores data on so we will mirror our drives. In another tutorial we will create off-site backups (S3 or gluster georeplication).
- [dnf-plugins-core](https://stackoverflow.com/questions/40937056/dnf-missing-config-manager-command) should add the [config-manager](https://kifarunix.com/install-and-setup-glusterfs-storage-cluster-on-centos-8/)
- gluster tut [here](https://www.itzgeek.com/how-tos/linux/centos-how-tos/install-and-configure-glusterfs-on-centos-7-rhel-7.html) also quickstart

```
dnf install dnf-plugins-core
dnf update
dnf install centos-release-gluster
vi ./CentOS-Gluster-9.repo
# replace mirrorlist with vault
dnf swap centos-linux-repos centos-stream-repos
dnf swap centos-linux-repos centos-stream-repos
reboot
dnf install glusterfs glusterfs-fuse glusterfs-server
service glusterd start
# service glusterd status
# gluster volume create gv0 replica 2 thorgluster:/mountpoint/gluster01 thorgluster:/mountpoint/gluster02
# gluster volume start gv0
# gluster volume info
```

### Test the GlusterFS volume

- TODO


### Adding Another Node

```
# iptables -I INPUT -p all -s 192.168.1.227 -j ACCEPT
# gluster peer probe server2
# gluster peer status
```

## __Part 5__: Snapshots

- Gluster Desaster recovery demo: https://www.gluster.org/glusterfs-disaster-recovery/