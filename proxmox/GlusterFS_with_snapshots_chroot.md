# GlusterFS Inside a Chroot

## Why Gluster

- __WARNING__: XFS requires a battery backup (UPS)

- GlusterFS is an easy to use a distributed file system that we will link to Kubernetes through a storage class.
  - GlusterFS simpler than Ceph and has lower resource requirements.
  - This setup shouldn't require large amounts of additional RAM like a ZFS setup would.
  - Distributed file systems allow you to use storage from any machine on any other machine.
  - Snapshots. Georeplication in a future tutorial maybe
  - Gluster Quickstart: https://docs.gluster.org/en/latest/Quick-Start-Guide/Quickstart/
    - [Snapshots docs](https://rajeshjoseph.gitbooks.io/test-guide/content/features/chap-Managing_Snapshots.html)

- 3 part Tutorial:
  1. Create a Chroot
  2. Setting up Disks
  3. Installing Gluster Inside the Chroot

## __Part 1__: Create a Chroot

### Why Chroot

- Chroot changes the root directory of the current process and all its children. Unlike containers (docker or lxc), chroots don't isolate any of the Linux namespaces (cgroups, IPC, Network, PID, UTS, User? and Mount).
- Using chroots means we can isolate gluster's dependencies and errors from proxmox, but still allows gluster to manage system resources like networking and disks.
- https://help.ubuntu.com/community/BasicChroot
- scatman - https://github.com/89luca89/scatman

### Remove old Chroot

- [stop the chroot](https://askubuntu.com/questions/893978/removing-a-chroot-environment#894026):

```cmd
# list all sessions:
schroot --list --all-sessions
# if the above command does not work, just type `mount`. The bind mount
# points with something like this in the mount path is the session name you want to get:
precise-a4aac8e0-663c-4fec-8fb2-16e4b06557e3 (<chroot_name>-<id>)

# now run this to properly end the session:
schroot --end-session --chroot=precise-ca6c72e4-0e9f-4721-8a0e-cca359e2c2fd
```

- Remove chroot configs in `/etc/schroot/schroot.conf`
- Remove mounts in `/etc/fstab`

### Installing a Chroot

- Install dependencies

```cmd
# apt install debootstrap schroot lvm2
```

- Create a config file for the chroot in `/etc/schroot/schroot.conf`.
  - run `man schroot.conf` for docs

```ini
# schroot ini file
[gluster]
description=Gluster server chroot
location=/root/glusterchroot
root-users=root
root-groups=root
```

- Create the chroot

```cmd
# mkdir /root/glusterchroot
# source /etc/os-release
# debootstrap --no-check-certificate --variant=buildd --arch i386 $VERSION_CODENAME /root/glusterchroot http://deb.debian.org/debian/
```

- Verify that it works

```cmd
# chroot /root/glusterchroot
# exit
```

### Mounting Resources to the Chroot with `Fstab`

- fstab rules in `/etc/fstab`

```fstab
sys /root/glusterchroot/sys sysfs defaults 0 1
proc /root/glusterchroot/proc proc defaults 0 0
/etc/resolv.conf /root/glusterchroot/etc/resolv.conf none bind,ro
/dev/ /root/glusterchroot/dev none bind,create=file 0 0
/run/lvm /root/glusterchroot/run/lvm none bind,create=file 0 0
/run/udev /root/glusterchroot/run/udev none bind,create=file 0 0
#/etc/hosts /root/glusterchroot/etc/hosts none bind,ro
```

- reboot or run `mount -a && mount`

### Use LVM inside the Chroot

```
# chroot /root/glusterchroot
# apt install lvm2
# lvscan
# vgscan
# pvscan
```

## __Part 2__: Setting up Disks

- __Architecture__: 1 disk for the OS (SSD), 1 disk for VM storage, and the rest provisioned with XFS on LVM for gluster. Also proxmox backup server, not covered here.
- This architecture avoids memory-heavy file systems while enabling snapshots and distributed storage.
- https://rajeshjoseph.gitbooks.io/test-guide/content/features/chap-Managing_Snapshots.html

### Wiping the disks

- Use `fdisk -l` to see what disks you have.
- Also check `pvscan`. Use `pvremove` if necessary.
- for information about the disks:

```sh
smartctl -i --test=short /dev/sda
```

- Unmount old file systems on the glusterFS disks with `umount /dev/mapper/...`
- Use `wipefs -a /dev/sdd` to remove old file systems.
- Remove old partitions with `sgdisk --zap-all /dev/sdX`
- partition with `gdisk` then `o` then `w`

### Create VM Storage

- Use lvmthin because you can do snapshots.
- Dedicate a disk entirely to storing VM images and LXC container templates.
  - Easily done through GUI.
  - `pvesm scan lvmthin`
  - To delete a stroage pool run `pvesm remove <pool name>`

<!--
This line is useful for storage mounting but not for what we're doing here.
pvesm add lvmthin glusterlvmthinsdd --thinpool thpl --vgname vgrp
-->

### LVM thin provision disks

- Needed so we can make snapshots at the Gluster level.
- Create physical volumes

```
# pvscan
# pvcreate --dataalignment 256k /dev/sdc
# pvscan
```

- Create volume groups

```
# vgscan
# vgcreate glustersdcvgrp /dev/sdc
# vgscan
```

- Create logical volumes (poolmetadatasize should be at least .5% of size)

```
# lvscan
# lvcreate --size 1.7T --thin glustersdcvgrp/glustersdcpool --chunksize 256k --poolmetadatasize 15G  --zero n
# lvcreate --virtualsize 1.7T --thin glustersdcvgrp/glustersdcpool --name glustersdclv
# lvscan
```

<!-- vgchange -a y vgrp (https://unix.stackexchange.com/questions/11125/lvm-devices-under-dev-mapper-missing) -->

- `/dev/glustersdcvgrp` is created.

### Formatting Disks with XFS

- Steps from gluster quickstart.

```
# findmnt
# mkfs.xfs -i size=512 /dev/glustersdcvgrp/glustersdclv
# mkdir --parents /root/glusterchroot/mnt/brick1-sdc
# echo '/dev/glustersdcvgrp/glustersdcthpl /root/glusterchroot/mnt/brick1-sdc xfs defaults 1 2' >> /etc/fstab
# mount -a && mount
# findmnt
# ls /root/glusterchroot/mnt/
```

### Verify Everything

```
# chroot /root/glusterchroot
# lvscan
# ls /mnt/
```
<!-- 
### Test LVM snapshots

```
touch /mnt/glustersdc/before_snapshot
```

```
lvscan
lvcreate --size 150 --snapshot --name backup glustersdcvgrp/glustersdclv
lvscan
```

```
mv /mnt/glustersdc/before_snapshot /mnt/glustersdc/after_snapshot
```

- TODO: restore

```
pass
```

- TODO: un-restore

- delete the old snapshot

```
lvremove backup
``` 
-->

## __Part 3__: Installing Gluster Inside the Chroot

<!-- - Proxmox doesn't backup drives gluster stores data on so we will mirror our drives. In another tutorial we will create off-site backups (S3 or gluster georeplication). -->

### Error fix from part 2

- add the following line to `/etc/fstab`

```
/run/udev /root/glusterchroot/run/udev none bind,create=file 0 0
```

- make sure to mount proc into `/root/glusterchroot/proc`

### Enable LVM Snapshots in the Kernel

- Exit the chroot
- Add `dm-snapshot` to `/etc/modules-load.d/modules.conf` to enable the lvm snapshot utility that gluster uses to make snapshots.
  - Note, this is necessary because LVM can't enable the kernel module itself from within a chroot.
- If you don't do this you will see an error saying `snapshot: Required device-mapper target(s) not detected in your kernel.` when try to make an lvm snapshot with the following command from within a chroot.
 
```
# lvcreate --size 150 --snapshot --name backup glustersdcvgrp/glustersdclv
# lvremove glustersdcvgrp/backup
```

### Install Gluster

```
# chroot /root/glusterchroot
# apt upgrade
# apt install glusterfs-server
# apt install software-properties-common
```

https://docs.gluster.org/en/latest/Install-Guide/Install/#community-packages

### Enable the Gluster Service


#### Edit `glusterd.service`

- Add the following lines to `/root/glusterchroot/lib/systemd/system/glusterd.service` under `[Service]`

```
# ***** Required *****
RootDirectory=/root/glusterchroot
GuessMainPID=1
# ***** Optional *****
SyslogIdentifier=glusterchroot
StandardError=journal
ProtectClock=yes
ProtectKernelTunables=1
ProtectKernelModules=1
LockPersonality=1
```
<!-- 
```
[Install]
WantedBy=multi-user.target
``` -->

- Comment out or remove the line `PIDFile=/run/glusterd.pid`

#### Symlink to `glusterd.service`

- Create the following symlink so the host and the chroot have the same service file.

```
# ln -s /root/glusterchroot/lib/systemd/system/glusterd.service /lib/systemd/system
```

#### Start `glusterd`

- Run the following commands

```
# systemctl enable glusterd.service
# systemctl start glusterd.service
# systemctl status glusterd.service
```

### Configure Gluster

- If you have multiple nodes allow them to communicate with each other with the following command (on the host):

```
# iptables -I INPUT -p all -s <ip-address> -j ACCEPT
```

- Chroot in for the rest of this section
- Add new nodes with `gluster peer probe server2`
  - important
- Add the following to `/etc/glusterfs/glusterd.vol`

```
option rpc-auth-allow-insecure on
```

- Run the following commands

```
# gluster volume create thor-gv0 replica 2 thor:/mnt/brick1-sdc thor:/mnt/brick2-sdd force
# gluster volume set thor-gv0 server.allow-insecure on
# gluster volume start thor-gv0
# gluster volume info
```

- Make sure `/var/log/glusterfs/glusterd.log` shows thor-gv0 is started.

##### ERROR

```
root@thor:/# gluster volume create thor-gv0 replica 2 thorgluster:/mnt/brick1-sdc thorgluster:/mnt/brick2-sdd force
volume create: thor-gv0: failed: Host thorgluster is not in 'Peer in Cluster' state
```

### Test Gluster

- Exit the chroot

```
mount -t glusterfs thorgluster:/thor-gv0 /mnt
```

#### Test Snapshots

- https://docs.gluster.org/en/latest/Administrator-Guide/Managing-Snapshots/
- pause geo-replication if necessary with the following command:

```
# gluster volume geo-replication MASTER_VOL SLAVE_HOST::SLAVE_VOL pause
```

- Also stop hadoop if it's running
- Snapshot the master volume before snapshotting slave volumes
- To create a snapshot run:

```
# gluster snapshot create <snapname> <volname> [no-timestamp] [description <description>] [force]
```

- snapshots are mounted read-only at `/var/run/gluster/snaps`???
- List the snapshots:

```
# gluster snapshot list [volname]
# gluster snapshot info [(<snapname> | volume VOLNAME)]
# gluster snapshot status [(<snapname> | volume VOLNAME)]
```

### Error: Failed to get pool name

- [gluster line](https://github.com/gluster/glusterfs/blob/05b02649036407acde52eb59aa564e5f42dfaeab/xlators/mgmt/glusterd/src/snapshot/glusterd-lvm-snapshot.c#L167)

- error text from `cat /var/log/glusterfs/glusterd.log | grep 2022-12-17 | grep -v " D "`:

```
[2023-01-22 13:05:14.418678 +0000] D [MSGID: 0] [glusterd-locks.c:657:glusterd_mgmt_v3_lock] 0-management: Lock for thor-gv0_vol successfully held by 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3
[2023-01-22 13:05:14.418691 +0000] D [MSGID: 0] [glusterd-locks.c:565:glusterd_mgmt_v3_lock] 0-management: Trying to acquire lock of snap1_GMT-2023.01.22-13.05.14_snap for 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3
[2023-01-22 13:05:14.418757 +0000] D [MSGID: 0] [glusterd-locks.c:657:glusterd_mgmt_v3_lock] 0-management: Lock for snap1_GMT-2023.01.22-13.05.14_snap successfully held by 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3
[2023-01-22 13:05:14.418767 +0000] D [MSGID: 0] [glusterd-locks.c:512:glusterd_multiple_mgmt_v3_lock] 0-management: Returning 0
[2023-01-22 13:05:14.418809 +0000] D [rpc-clnt-ping.c:296:rpc_clnt_start_ping] 0-management: ping timeout is 0, returning
[2023-01-22 13:05:14.419433 +0000] D [MSGID: 0] [glusterd-mgmt.c:760:glusterd_mgmt_v3_initiate_lockdown] 0-management: Sent lock op req for Snapshot to 1 peers. Returning 0
[2023-01-22 13:05:14.419460 +0000] D [MSGID: 0] [glusterd-utils.c:1781:glusterd_volinfo_find] 0-management: Volume thor-gv0 found
[2023-01-22 13:05:14.419467 +0000] D [MSGID: 0] [glusterd-utils.c:1788:glusterd_volinfo_find] 0-management: Returning 0
[2023-01-22 13:05:14.419474 +0000] D [MSGID: 0] [glusterd-snapshot.c:2412:glusterd_snapshot_create_prevalidate] 0-management: snap-max-hard-limit is not present in opts dictionary
[2023-01-22 13:05:14.419547 +0000] D [run.c:242:runner_log] (-->/usr/lib/i386-linux-gnu/glusterfs/9.2/xlator/mgmt/glusterd.so(+0xc7985) [0xf3861985] -->/usr/lib/i386-linux-gnu/glusterfs/9.2/xlator/mgmt/glusterd.so(+0xc5b1c) [0xf385fb1c] -->/usr/lib/i386-linux-gnu/libglusterfs.so.0(runner_log+0xf6) [0xf7e6d9a6] ) 0-management: Get thin pool name for device /dev/mapper/glustersdcvgrpglustersdclv: lvs --noheadings -o pool_lv /dev/mapper/glustersdcvgrpglustersdclv
[2023-01-22 13:05:14.496508 +0000] E [MSGID: 106077] [glusterd-snapshot.c:1903:glusterd_is_thinp_brick] 0-management: Failed to get pool name for device /dev/mapper/glustersdcvgrpglustersdclv
[2023-01-22 13:05:14.496569 +0000] E [MSGID: 106657] [glusterd-snapshot.c:2039:glusterd_snap_create_clone_common_prevalidate] 0-management: Snapshot is supported only for thin provisioned LV. [{Ensure that all bricks of volume are thinly provisioned LV, Volume=thor-gv0}]
[2023-01-22 13:05:14.496584 +0000] E [MSGID: 106121] [glusterd-snapshot.c:2454:glusterd_snapshot_create_prevalidate] 0-management: Failed to pre validate
[2023-01-22 13:05:14.496591 +0000] E [MSGID: 106024] [glusterd-snapshot.c:2471:glusterd_snapshot_create_prevalidate] 0-management: Snapshot is supported only for thin provisioned LV. Ensure that all bricks of thor-gv0 are thinly provisioned LV.
[2023-01-22 13:05:14.496600 +0000] W [MSGID: 106029] [glusterd-snapshot.c:8628:glusterd_snapshot_prevalidate] 0-management: Snapshot create pre-validation failed
[2023-01-22 13:05:14.496606 +0000] W [MSGID: 106121] [glusterd-mgmt.c:151:gd_mgmt_v3_pre_validate_fn] 0-management: Snapshot Prevalidate Failed
[2023-01-22 13:05:14.496612 +0000] D [MSGID: 0] [glusterd-mgmt.c:244:gd_mgmt_v3_pre_validate_fn] 0-management: OP = 28. Returning -1
[2023-01-22 13:05:14.496618 +0000] E [MSGID: 106121] [glusterd-mgmt.c:1043:glusterd_mgmt_v3_pre_validate] 0-management: Pre Validation failed for operation Snapshot on local node
[2023-01-22 13:05:14.496625 +0000] E [MSGID: 106121] [glusterd-mgmt.c:2946:glusterd_mgmt_v3_initiate_snap_phases] 0-management: Pre Validation Failed
[2023-01-22 13:05:14.496730 +0000] D [rpc-clnt-ping.c:296:rpc_clnt_start_ping] 0-management: ping timeout is 0, returning
[2023-01-22 13:05:14.497060 +0000] D [MSGID: 0] [glusterd-mgmt.c:2255:glusterd_mgmt_v3_post_validate] 0-management: Sent post valaidation req for Snapshot to 1 peers. Returning 0
[2023-01-22 13:05:14.497093 +0000] D [rpc-clnt-ping.c:296:rpc_clnt_start_ping] 0-management: ping timeout is 0, returning
[2023-01-22 13:05:14.497430 +0000] D [MSGID: 0] [glusterd-mgmt.c:2444:glusterd_mgmt_v3_release_peer_locks] 0-management: Sent unlock op req for Snapshot to 1 peers. Returning 0
[2023-01-22 13:05:14.497458 +0000] D [MSGID: 0] [glusterd-locks.c:785:glusterd_mgmt_v3_unlock] 0-management: Trying to release lock of vol thor-gv0 for 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3 as thor-gv0_vol
[2023-01-22 13:05:14.497476 +0000] D [MSGID: 0] [glusterd-locks.c:835:glusterd_mgmt_v3_unlock] 0-management: Lock for vol thor-gv0 successfully released
[2023-01-22 13:05:14.497488 +0000] D [MSGID: 0] [glusterd-utils.c:1781:glusterd_volinfo_find] 0-management: Volume thor-gv0 found
[2023-01-22 13:05:14.497495 +0000] D [MSGID: 0] [glusterd-utils.c:1788:glusterd_volinfo_find] 0-management: Returning 0
[2023-01-22 13:05:14.497504 +0000] D [MSGID: 0] [glusterd-locks.c:785:glusterd_mgmt_v3_unlock] 0-management: Trying to release lock of snap snap1_GMT-2023.01.22-13.05.14 for 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3 as snap1_GMT-2023.01.22-13.05.14_snap
[2023-01-22 13:05:14.497514 +0000] D [MSGID: 0] [glusterd-locks.c:835:glusterd_mgmt_v3_unlock] 0-management: Lock for snap snap1_GMT-2023.01.22-13.05.14 successfully released
[2023-01-22 13:05:14.497521 +0000] D [MSGID: 0] [glusterd-utils.c:1788:glusterd_volinfo_find] 0-management: Returning -1
```

- Fix attempt no 1: remove `-` from all pools and lvs
  - doesn't work

- Fix attempt no 2:
  - 


#### Test Gluster Volume

#### Adding Another Node

```
# iptables -I INPUT -p all -s 192.168.1.227 -j ACCEPT
# gluster peer probe server2
# gluster peer status
```

### Troubleshooting

- `cat ./glusterchroot/var/log/glusterfs/glusterd.log`
- https://docs.gluster.org/en/main/Troubleshooting/troubleshooting-glusterd/#debugging-glusterd

```
man systemd.service
man systemd.exec
man systemd.syntax
```

- `journalctl -g gluster --boot`

### Future Work

- nspawn

- create a deb package of glusterchroot and a commandline utility that can:
  - create the chroot and configure everything when installed
  - Enter the chroot
  - manage disks (add/remove/list and update fstab)
  - manage snapshots (add/remove/list and update fstab)
  - Have auto-complete
  - maybe use [bashly](https://github.com/DannyBen/bashly)
  - [Stack OF post on packaging deb](https://askubuntu.com/questions/1345/what-is-the-simplest-debian-packaging-guide)