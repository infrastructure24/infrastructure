
# useful storage commands

```
smartctl -i -d cciss,3 --test=short /dev/sda
smartctl -i -d cciss,2 --test=short /dev/sdb
smartctl -i -d cciss,1 --test=short /dev/sdc
smartctl -i -d cciss,0 --test=short /dev/sdc
```


- On the backup server (not here!) you can use:

```
proxmox-backup-manager disk initialize sdc
proxmox-backup-manager disk fs create store02_900GB --disk sdc --filesystem ext4 --add-datastore true
```