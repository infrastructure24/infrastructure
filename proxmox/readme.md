# intro

- Using proxmox to manage nodes. Using proxmox backup server to back up the nodes.
- kubernetes
- Glusterfs vs ceph

# getting started

## Install from ISO

- Nothing special. Etcher like always.
- link: https://www.proxmox.com/en/downloads

## Make sure networking works

- Ping the default gateway. If works than ping 8.8.4.4 (Google DNS server) if successfully pinged both you have internet access but no dns name resolving which is your problem i think. In the web gui go to dns settings and edit servers. Change address to Google Public DNS server. After do a systemctl restart networking command. If networking still fails, edit the /etc/networking/interfaces and hardcode dns addresses.
- In my case DNS wasn't working on the backup server so I had to add nameservers to `/etc/resolv.conf`

## `apt update`

- For apt update to work you need to use the apt repo that doesn't require a subscription. These steps describe how to switch to the non-subscription repo.

- remove pbs-enterprise.list

```
mv ./sources.list.d/pbs-enterprise.list pbs-enterprise.list.bak
```

- Replace the content in `/etc/apt/sources.list` with the following.

```
deb http://ftp.debian.org/debian bullseye main contrib
deb http://ftp.debian.org/debian bullseye-updates main contrib

# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve bullseye pve-no-subscription

# security updates
deb http://security.debian.org/debian-security bullseye-security main contrib
```

- Note, I'm using debian bullseye. You if you're using a different debian version, you may need to replace bullseye with your version. For debian versions see here `https://192.168.1.227:8006/pve-docs/pve-admin-guide.html#_frequently_asked_questions_2`

## More Things

- use `root@pam` as backup server user




## Terraform for Kubernetes

- pass

## 