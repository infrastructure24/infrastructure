#! /bin/bash

# rbac rules
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-rbac.yaml --wait=true

# creating the traefik daemonset
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-ds.yaml --wait=true

# check that it worked
#kubectl --namespace=kube-system get pods


# create an ingress rule for the traefik ui
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/ui.yaml

# add a rule to route traffic to traefik from this machine
#echo "192.168.1.42 traefik-ui.minikube" | sudo tee -a /etc/hosts


