# notes on deploying traefik

- [here are the docs on deploying traefik in k8s](https://docs.traefik.io/v1.7/user-guide/kubernetes/)
- https://stackoverflow.com/questions/50276391/treafik-ui-kubernetes-connection-refused
- [my ingress notes](https://github.com/user-name-is-taken/learning_kubernetes/blob/master/ch5_services/ch5_notes.md)
- [official kubic stuff](https://kubic.opensuse.org/blog/2019-01-24-traefik/)
- It turned out the trick was making my ingress NodePort instead of ClusterIP


# Ingress docs

- This tutorial covers how to deploy [Traefik](https://docs.traefik.io/), an ingress controller for kubernetes in our infrastructure.
- [Ingress controllers](https://kubernetes.io/docs/concepts/services-networking/ingress/#what-is-ingress) are LoadBalancer services that route HTTP and HTTPS requests from outside the cluster to services inside the cluster based on the domain name in the request's headers.
- This tutorial is cook-book style summary for our infrastructure of a longer [video tutorial by Rancher](https://www.youtube.com/watch?v=Ytc24Y0YrXE).


# Reserve a DHCP range in the Router

- First, reserve a DHCP range in your router for MetalLb to assign to LoadBalancer services (like the Ingress service).

## PFSense example

- Go into `Services/DHCP Server/ <your interface's name>`
- Enable DHCP by checking the `Enable` checkbox
- Assign a range that this dhcp server can assign addresses on in the `range` section. Make sure this DHCP range doesn't overlap with existing DHCP ranges.

# Deploy a network Load Balancer

- [Layer 2 network load balancers](https://metallb.universe.tf/concepts/layer2/) assign IP Addresses from the reserved DHCP range to [LoadBalancer services](https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/) in the cluster using ARP.

## Metallb Example

- Installing metallb's components using the following commands from the [metallb install docs](https://metallb.universe.tf/installation/#installation-by-manifest)

```sh
# deploy a namespace
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.9.3/manifests/namespace.yaml
# deploy metallb's RBAC, speaker and controller components
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.9.3/manifests/metallb.yaml
# create secrets 
# On first install only
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

- Tell metallb about the DHCP range you set with a configmap named `config.yaml` as described in the [config docs](https://metallb.universe.tf/configuration/#layer-2-configuration) and shown below.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 192.168.1.240-192.168.1.250 
#replace this range with the DHCP IP range you specified in the router
```

- Create the configmap resource in kubernetes

```sh
$ kubectl create -f ./config.yamls
```

## Metallb with Helm

- If you install metallb with helm, you must name your configmap `metallb-config.yaml`, instead of `config.yaml`

# Ingress

- [Ingress controllers](https://kubernetes.io/docs/concepts/services-networking/ingress/#what-is-ingress) are LoadBalancer services that route HTTP and HTTPS requests from outside the cluster to services inside the cluster based on the domain name in the request's headers.
- This section covers installing an ingress controller named traefik using helm.

## Installing Traefik

- Custom values that override defaults from [here](https://github.com/helm/charts/tree/master/stable/traefik)

```yaml
kubernetes:
  # endpoint:
  ingressEndpoint:
    # hostname: "localhost"
    useDefaultPublishedService: true

dashboard:
## Add arbitrary ConfigMaps to deployment
  enabled: true
  domain: traefik.greenhouseaffectors.com
  auth:
    basic:
      user: password
  
rbac:
  enabled: true
```

- Run the following commands to install Traefik.

```sh
$ vim values.yaml
# add the yaml above to the file
$ kubectl create namespace treafik-system
$ helm repo add stable https://kubernetes-charts.storage.googleapis.com
$ helm install traefik-release stable/traefik --values ./values.yaml --namespace traefik-system 
```

## Testing Traefik

- See that the helm chart created a load balancer service for the ingress, and the service has an external IP. 
  - *assigning the external IP may take a few minutes*

```
$ kubectl get services --namespace traefik-system
```

- See that the helm chart created the ingress rule you specified (`traefik.greenhouseaffectors.com`) for the traefik dashboard.

```
$ kubectl get ingress --namespace traefik-system
```
 
- See that the ingress controller works by:
  - Adding a DNS rule `/etc/hosts` that resolves to the external IP of the ingress controller
  - Going to the hostname specified by the ingress rule in the browser

```sh
$ echo "172.16.10.241   traefik.greenhouseaffectors.com" | sudo tee --append /etc/hosts
# replace 172.16.10.241 with traefik's external IP and traefik.greenhouseaffectors.com with traefik's hostname
# now visit traefik.greenhouseaffectors.com in the browser
```

# Assign DNS rules in the router

- TODO