# Traefik install instructions

- [Values ref](https://github.com/traefik/traefik-helm-chart/blob/master/traefik/values.yaml)

```
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
kubectl create ns traefik
helm upgrade --values ./values2.yaml --namespace traefik --install --atomic traefik traefik/traefik
```

- 