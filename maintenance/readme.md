# readme

- To resolve [this issue](https://github.com/thkukuk/kubic-control/issues/32) I had to do [the following](https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.kubeadm.html) command:

`salt '*' kubeadm.alpha_certs_renew`

- [Docs on updating certs](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init-phase/)

```
kubeadm init phase certs all --config kubeadm.yaml
```

- hitting this now: https://stackoverflow.com/questions/36939381/x509-certificate-signed-by-unknown-authority-kubernetes

- Have to update `~/.kube/config` also


```
$ kubeadm init phase certs apiserver --apiserver-cert-extra-sans greenhouseaffectors.com
$ kubeadm init phase certs apiserver-kubelet-client
```

```
kubeadm config images pull
kubeadm upgrade plan
kubeadm upgrade apply v1.21.1
```


- https://github.com/openSUSE/cooverview/issues/12
 - need to check locally for image first???

- video: https://www.youtube.com/watch?v=DhsFfNSIrQ4

- etcdctl steps: https://github.com/etcd-io/etcd/issues/10642
  - https://stackoverflow.com/questions/63289800/unable-to-check-etcd-cluster-health

```
# etcdctl snapshot save snapshot.db \
  --cacert /etc/kubernetes/pki/etcd/ca.crt \
  --cert /etc/kubernetes/pki/etcd/server.crt \
  --key /etc/kubernetes/pki/etcd/server.key
```

```
# etcdctl --write-out=table snapshot status snapshot.db
```

```
etcdctl member list   --cacert /etc/kubernetes/pki/etcd/ca.crt   --cert /etc/kubernetes/pki/etcd/server.crt   --key /etc/kubernetes/pki/etcd/server.key
```

- I think kubernetes (kubeadm) is using the wrong cert to talk to etcd


- https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/