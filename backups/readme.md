# readme

- [volume snapshot classes docs](https://kubernetes.io/docs/concepts/storage/volume-snapshot-classes/#driver)
  - [nfs csi driver](https://github.com/kubernetes-csi/csi-driver-nfs)
  - [other drivers](https://kubernetes-csi.github.io/docs/drivers.html)
  - [volume snapshot docs](https://kubernetes.io/docs/concepts/storage/volume-snapshots/)
  - [external snapshotter](https://github.com/kubernetes-csi/external-snapshotter/tree/master#usage)!!!
  - [nfs provisioner](https://github.com/kubernetes-sigs/nfs-ganesha-server-and-external-provisioner)

- actual steps
  - [install csi-driver-nfs with helm](https://github.com/kubernetes-csi/csi-driver-nfs/blob/master/deploy/example/README.md)
    - Need to create a service account?
  - [create a VolumeSnapshotClass using csi-driver-nfs](https://kubernetes.io/docs/concepts/storage/volume-snapshot-classes/)
    - [storage class params example](https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/deploy/example/storageclass-nfs.yaml)

```yaml
apiVersion: snapshot.storage.k8s.io/v1
kind: VolumeSnapshotClass
metadata:
  name: csi-hostpath-snapclass
driver: nfs.csi.k8s.io
deletionPolicy: Delete
parameters:
  server: nfs-server.default.svc.cluster.local # change me
  share: /
```





# Other options

- [velero docs](https://velero.io/docs/v1.5/)
  - requires an s3 backend (which I could do at the nas?)
  - owned by vmware

- appcode stash
  - required s3 for community version

- [netapp trident](https://netapp-trident.readthedocs.io/en/stable-v21.01/introduction.html)

- deploying your own s3 storage with [trueNAS]
  - maybe easyNAS would work?