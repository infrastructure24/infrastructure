# pass

- pass

# pre-reqs

- check pre-reqs with:

```
$ curl https://docs.kasten.io/tools/k10_primer.sh | bash
```

## Install volume snapshot

### Install the CRDs

- [volume snapshot APIs](https://kubernetes-csi.github.io/docs/snapshot-restore-feature.html)
  - https://kubernetes-csi.github.io/docs/snapshot-controller.html

```
https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/client/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml


kubectl create -f  https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-3.0/client/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml

kubectl create -f  https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-3.0/client/config/crd/snapshot.storage.k8s.io_volumesnapshotcontents.yaml

kubectl create -f  https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/release-3.0/client/config/crd/snapshot.storage.k8s.io_volumesnapshots.yaml
```

### Create a VolumeSnapshotClass

- [Use cephfs](https://rook.io/docs/rook/v1.5/ceph-csi-snapshot.html)
- [CRDs](https://github.com/rook/rook/tree/release-1.5/cluster/examples/kubernetes/ceph/csi/cephfs)

```
kubectl apply -f .\cephfs-volumesnapshotclass.yaml
```


## apply the helm

```
$ kubectl create secret generic -n kasten-io --from-file ./.htpasswd
$ helm repo add kasten https://charts.kasten.io/
$ kubectl create ns kasten-io
$ helm install k10 kasten/k10 --namespace=kasten-io -f ./values.yaml
```

## Access the website

- Make sure to include the path!!!

`k10.greenhouseaffectors.com/k10/#/`