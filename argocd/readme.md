# argo

- https://argoproj.github.io/argo-cd/getting_started/#2-download-argo-cd-cli

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

- https://argoproj.github.io/argo-cd/operator-manual/ingress/#traefik-v22

```
kubectl apply -f ./ingressRoute.yaml
```