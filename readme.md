# kubic stuff

- [kubicctl quickstart](https://kubic.opensuse.org/blog/2019-08-27-kubic-control-intro/)
  - [salt minion configuration](https://docs.saltstack.com/en/latest/ref/configuration/minion.html)
  - need to do more with the salt minion.
- [kubelet.conf location](https://stackoverflow.com/questions/52941015/how-to-find-master-node-from-worker-node-in-kubernetes)
- [main kubicctl repo](https://github.com/thkukuk/kubic-control)
- nodes added to the cluster
  - salt minion added https://www.youtube.com/results?search_query=saltstack+basic+use+pipe2grep
  - `kubic node add new-host-20`...
- [kubic: kubicD and kubicctl](https://en.opensuse.org/Kubic:KubicD_and_kubicctl)

## gitlab stuff

- [linking gitlab and kubic](https://medium.com/joolsoftware/connect-gitlab-with-digitaloceans-kubernetes-439076b9de17)
- need ingress? https://kubic.opensuse.org/blog/2019-01-24-traefik/
