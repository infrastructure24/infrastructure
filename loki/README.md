# loki

```sh
$ helm repo add loki https://grafana.github.io/loki/charts

$ helm repo update

$ helm upgrade --namespace loki --install --values ./values.yaml loki loki/loki-stack
```

- 